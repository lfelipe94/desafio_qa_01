package page_object;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import comandos.Comandos;
import driver.DriverBuilder;

public class PhpTravelPage extends Comandos {

	protected List<String> dados = new ArrayList<String>();
	
	public PhpTravelPage(DriverBuilder driver) {
		super(driver);
	}

	public void login(String usuario, String senha) {
		escreve("email", usuario);
		escreve("password", senha);
		encontra("password").submit();
	}

	public void adiciona_room() throws InterruptedException {
		aguarde_presenca("Add Room");
		clica("Add Room");
		preenche_general();
		preenche_amenites();
		clica("add");
	}
	
	
	public void preenche_general() throws InterruptedException {
		aguarde_presenca("General");
		escolhe_opcao("roomstatus");
		clica("s2id_autogen1");
		escolher_opcoes_definidas();
		clica("s2id_autogen3");
		escolher_opcoes_definidas();
		
		aguarde("iframe");
		WebElement frame = driver.findElement(By.tagName("iframe"));
		driver.switchTo().defaultContent();
		driver.switchTo().frame(frame);
		aguarde("body");
		driver.findElement(By.tagName("body")).sendKeys("Teste do Luiz");
		driver.switchTo().defaultContent();
		
		escreve("basicprice",Integer.toString(numero_aleatorio()));
		escreve("quantity",Integer.toString(numero_aleatorio()));
		escreve("minstay",Integer.toString(numero_aleatorio()));
		escreve("adults",Integer.toString(numero_aleatorio()));
		escreve("children",Integer.toString(numero_aleatorio()));
		escreve("extrabeds",Integer.toString(numero_aleatorio()));
		escreve("bedcharges",Integer.toString(numero_aleatorio()));
	}
	
	public void preenche_amenites() {
		driver.findElement(By.tagName("body")).sendKeys(Keys.PAGE_UP);
		clica("Amenities");
		aguarde("ins");
		List<WebElement> campos = driver.findElements(By.tagName("ins"));
		for (WebElement webElement : campos) {
			if(descisao_aleatoria()) {
				webElement.click();
			}			
		}
	}
	
	public void escolher_opcoes_definidas() throws InterruptedException {
		aguarde("ul");
		Thread.sleep(800);
		List<WebElement> u = driver.findElements(By.tagName("ul"));
		List<WebElement> l = u.get(6).findElements(By.tagName("li"));
		String texto = l.get(gerar_num_aleatorio(l)).getText();
		WebElement e = driver.findElement(By.id("select2-drop")).findElement(By.tagName("input"));
		e.sendKeys(texto);
		e.sendKeys(Keys.ENTER);
		dados.add(texto);
	}
	
	public WebElement verifica_room_reservada() {
		aguarde_presenca("Print");
		WebElement e = encontra(dados.get(0));
		return e;
	}
}