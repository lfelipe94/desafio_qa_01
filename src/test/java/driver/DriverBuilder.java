package driver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class DriverBuilder {

	private String caminhoDriver;
	private WebDriver driver;

	public DriverBuilder(String site) {

		// informa a localizacao do driver no projeto
		this.caminhoDriver = System.getProperty("user.dir") + "\\bin\\chromedriver.exe";

		// configura as propriedas do driver
		System.setProperty("webdriver.chrome.driver", caminhoDriver);

		// valida qual tipo de driver sera construido
		this.driver = new ChromeDriver();

		// maximiza e acessa o site
		this.driver.manage().window().maximize();
		this.driver.get(site);

	}
	
	public WebDriver getDriver() {
		return this.driver;
	}


	public void navega(String texto) {
		// navegar para outros links
		this.driver.navigate().to(texto);
	}

	public void encerra() {
		// Encerra o driver
		try {
			this.driver.close();
			this.driver.quit();			
		} catch (Exception e) {
			System.out.println("Nenhum Driver aberto");
		}
	}
}
