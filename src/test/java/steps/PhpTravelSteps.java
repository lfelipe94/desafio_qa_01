package steps;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebElement;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import driver.DriverBuilder;
import page_object.PhpTravelPage;

public class PhpTravelSteps {

	private DriverBuilder driver = new DriverBuilder("http://www.phptravels.net/supplier");
	private PhpTravelPage phptravel = null;

	@Given("^Acesse o site PHP Travel$")
	public void acesse_o_site_PHP_Travel() throws Throwable {
		// Inicializa��o do Driver e da Classe
		phptravel = new PhpTravelPage(this.driver);
	}

	@When("^Realizar login com sucesso$")
	public void realizar_login_com_sucesso() throws Throwable {
		// Login no site
		phptravel.login("supplier@phptravels.com", "demosupplier");
	}

	@Then("^Acessar a opcao de Hotel$")
	public void acessar_a_opcao_de_Hotel() throws Throwable {
		// Validando que est� na pagina inicial
		phptravel.aguarde_presenca("Hotels");
		phptravel.clica("Hotels");
	}

	@When("^Adicionar uma nova Room$")
	public void adicionar_uma_nova_Room() throws Throwable {
		// Adicao de room
		phptravel.adiciona_room();
	}

	@Then("^Nova Room adicionada$")
	public void nova_Room_adicionada() throws Throwable {
		//Validando room criada
		WebElement resultado = phptravel.verifica_room_reservada();
		assertTrue(resultado != null);
		driver.encerra();
	}
}
