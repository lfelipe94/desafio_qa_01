package comandos;

import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import driver.DriverBuilder;

public class Comandos {

	protected WebDriver driver;

	public Comandos(DriverBuilder driver) {
		this.driver = driver.getDriver();
	}

	public void aguarde(String elemento) {
		WebDriverWait wait = new WebDriverWait(this.driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated((By.tagName(elemento))));
	}

	public void aguarde_presenca(String elemento) {
		WebDriverWait wait = new WebDriverWait(this.driver, 60);
		wait.until(ExpectedConditions.presenceOfElementLocated((By.partialLinkText(elemento))));
	}

	public int gerar_num_aleatorio(List<WebElement> options) {
		int max = options.size();
		int min = 0;
		int index = new Random().nextInt((max - min) + 1) + min;
		if (index == max) {
			index = index - 1;
		}
		return index;
	}

	public int numero_aleatorio() {
		int max = 10;
		int min = 0;
		int index = new Random().nextInt((max - min) + 1) + min;
		return index;
	}
	
	public boolean descisao_aleatoria() {
		boolean i = new Random().nextBoolean();
		return i;
	}

	public void escolhe_opcao(String elemento) {
		WebElement element = this.encontra(elemento);
		List<WebElement> options = element.findElements(By.tagName("option"));
		int index = this.gerar_num_aleatorio(options);
		Select s = new Select(element);
		s.selectByIndex(index);
	}

	public WebElement encontra(String elemento) {
		WebElement e = null;

		try {
			e = this.driver.findElement(By.className(elemento));
		} catch (Exception alerta) {
		}

		try {
			e = this.driver.findElement(By.cssSelector(elemento));
		} catch (Exception alerta) {
		}

		try {
			e = this.driver.findElement(By.id(elemento));
		} catch (Exception alerta) {
		}

		try {
			e = this.driver.findElement(By.linkText(elemento));
		} catch (Exception alerta) {
		}

		try {
			e = this.driver.findElement(By.partialLinkText(elemento));
		} catch (Exception alerta) {
		}

		try {
			e = this.driver.findElement(By.name(elemento));
		} catch (Exception alerta) {
		}

		try {
			e = this.driver.findElement(By.tagName(elemento));
		} catch (Exception alerta) {
		}

		try {
			e = this.driver.findElement(By.xpath(elemento));
		} catch (Exception alerta) {
		}

		return e;
	}

	public void clica(String elemento) {
		WebElement e = this.encontra(elemento);
		e.click();
	}

	public void escreve(String elemento, String texto) {
		WebElement e = this.encontra(elemento);
		e.sendKeys(texto);
	}
}
